
require 'socket'
require 'date'
require 'ap'
require 'sequel'
require 'logger'
require 'net/http'
require 'uri'

require './cpoint_saver'
require './efl_saver'

CODE_REGION = 25..35
VALUE_REGION = 36..40

$logger = Logger.new( "enfscan-#{Thread.current.object_id}.log", 'daily', :log_level => Logger::INFO)

puts "Enfora scaner (v.0.01)"

puts "Listening..."

udp_socket= UDPSocket.new
udp_socket.bind("92.47.27.114", 5203)

saver = CPointSaver.new
efl_saver = EFLSaver.new

@uri = URI.parse 'http://192.168.10.8:9010/server/status'
@urisvc = URI.parse 'http://89.218.150.214:2522'

	def post_message( msg )
		print "\n Sending data to Azimbek..."
		UDPSocket.open.send( msg, 0, '89.218.150.214', 2522)
		puts "done"
	rescue
			ap "Error::" + $!.message
			$logger.error $!.message
	end

	def post_error( msg )
			Net::HTTP.post_form( @uri, {"name" => "enfora scaner", "status" => "error", "data" => msg } )
	rescue
			ap "Error::" + $!.message
			$logger.error $!.message
	end

## MAIN LOOP ##
#############
loop do
	begin
		text, sender = udp_socket.recvfrom(50)

		code = text[CODE_REGION].to_i
		value = text[VALUE_REGION].to_i

		value /= 100.0

		date_str = DateTime.now.strftime("%Y-%m-%d %H:%M:%S")

		puts "#{date_str}::  code: #{code} value: #{value} :: request: #{text.size} bytes "
		print "Storing data..."
		saver.store_value( code, value, DateTime.now )
		efl_saver.store_value( code, value, DateTime.now )
		puts "ready"

		Net::HTTP.post_form( @uri, {"name" => "enfora scaner", "status" => "ok", "interval" => 180 } )
		post_message( text )
	rescue
			ap "Error::"
			ap $!.message
			$logger.error $!.message	     
			$logger.info $@[0..4]

			post_error( $!.message )
	end

end


upd_socket.close()

