class CPointSaver
	
	def store_value( device, value, date, factor = 6, coef = 1 )
		begin
			@db ||= connect_to_cpoint_db()

			value ||= 0.0
			coef = 1 if( coef < 1 )
			cvalue = value/coef
			cvalue ||= 0.0

			insert_ds = @db['INSERT INTO counters.dbo._Data (address, value, cvalue, factor_id, datetime) values (?,?,?,?,?)', device, cvalue, value, factor, date ]

			insert_ds.insert
			@db.disconnect unless @db.nil?
			@db = nil
		rescue
				$logger.info "Params for error: dev: #{device} val: #{value} date: #{date} factor: #{factor} coef: #{coef}"
				ap $@[0..6]
				ap $!.message

				$logger.error $!
				$logger.info $@
		end
	end

	def connect_to_cpoint_db
		Sequel.tinytds(
			user: 'puser', password: 'Win3Ts', host: '192.168.10.8', database: 'tanks', encoding: 'cp1251' 
			)
	end
end
