# Socket daemon replacemen
#
require 'date'
require 'ap'
require 'sequel'
require 'logger'
require 'uri'
require 'net/http'
require 'eventmachine'

require './cpoint_saver'
require './efl_saver'

COEF = 100

CODE_REGION = 1..4
VALUE_REGION = 16..19

$logger = Logger.new( "metscan-#{Thread.current.object_id}.log", 'daily', :log_level => Logger::INFO)

$efl_saver = EFLSaver.new

puts "Flowmeters scaner (v.0.01)"

puts "Listening..."

class Integer
	def to_hex_string
		("%0x" % self).size % 2 == 0 ? "%0x" % self : "%0#{("%0x" % self).size + 1}x" % self
	end
end

module FlowmeterServerEM 
	
	def post_init
		puts "Client connected"
	end

	def receive_data( text )
		begin
			close_connection 

			@uri ||= URI.parse("http://192.168.10.8:9010/server/status")

			saver = CPointSaver.new


			## Process data ##
			utext = text.unpack('C*').map{|v| v.to_hex_string() } 

			puts "Unpacked: #{utext.join(',')} :: len #{utext.size}"

			code = utext[CODE_REGION].reverse.join.to_i(16)
			value = utext[VALUE_REGION].reverse.join.to_i(16)

			date_str = DateTime.now.strftime("%Y-%m-%d %H:%M:%S")


			puts "#{date_str}::  code: #{code} cvalue: #{value/COEF} value: #{value} :: request: #{text.size} bytes "

			value ||= 0.0
			value = 0 if( value > 2_147_483_647 )

			## Store data ##


			print "Storing data..."
			saver.store_value( code, value, DateTime.now, 0, COEF ) #factor 0 = *100
			$efl_saver.store_value( code, value.to_f/COEF, DateTime.now )
			puts "ready"

			Net::HTTP.post_form( @uri, {"name" => "meters scaner", "status" => "ok", "interval" => 300} )
		rescue
			ap "Error::"
			ap $!.message
			$logger.error $!.message	     
			$logger.info $@[0..4]
		end
	end

	def unbind 
		puts "Client disconnected"
	end

	def post_error( msg )
			Net::HTTP.post_form( @uri, {"name" => "meters scaner", "status" => "error", "data" => msg } )
	rescue
			ap "Error::" + $!.message
			$logger.error $!.message
	end
end

EventMachine.run {
	EventMachine.start_server "92.47.27.114", 5202, FlowmeterServerEM
}
puts "Exited sock scaner"
